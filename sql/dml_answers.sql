-- INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id");

INSERT INTO answers(id, number, "body", "feeling_point", "question_id", "next_line_id", "next_question_id")
VALUES (1, 1, "杉本先生", "0", "1", "49", null);
INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id")
VALUES (2, 2, "健太", "0", "1", "71", "2");
INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id")
VALUES (3, 3, "桃", "3", "1", "104", "3");

INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id")
VALUES (4, 1, "じゃあ俺やってみるよ！", "0", "2", "82", "5");
INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id")
VALUES (5, 2, "お前やってみろよ！", "5", "2", "93", "5");

INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id")
VALUES (6, 1, "温かい飲み物を買ってくる", "3", "3", "120", "4");
INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id")
VALUES (7, 2, "聞こえなかった", "0", "3", "125", "4");

INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id")
VALUES (8, 1, "ライオン", "3", "4", "126", "5");
INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id")
VALUES (9, 2, "パンダ", "5", "4", "127", "5");
INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id")
VALUES (10, 3, "イグアナ", "10", "4", "128", "5");

INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id")
VALUES (11, 1, "話かける", "0", "5", "143", "6");
INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id")
VALUES (12, 2, "無視する", "0", "5", "143", "6");

INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id")
VALUES (13, 1, "ううん、別に", "0", "6", "148", "7");
INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id")
VALUES (14, 2, "可愛い", "6", "6", "149", "7");
INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id")
VALUES (15, 3, "無視", "2", "6", "150", "7");

INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id")
VALUES (16, 1, "手を繋ぐ", "5", "7", "210", "8");
INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id")
VALUES (17, 2, "腕を引っ張る", "3", "7", "213", "8");
INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id")
VALUES (18, 3, "先頭をきって進む", "0", "7", "215", "8");

INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id")
VALUES (19, 1, "頭を撫でる", "2", "8", "229", "9");
INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id")
VALUES (20, 2, "キスをする", "6", "8", "233", "9");
INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id")
VALUES (21, 3, "抱きしめる", "4", "8", "238", "9");

INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id")
VALUES (22, 1, "強く抱きしめる", "5", "9", "242", "10");
INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id")
VALUES (23, 2, "キスをする", "4", "9", "245", "10");
INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id")
VALUES (24, 3, "見つめる", "3", "9", "246", "10");

INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id")
VALUES (25, 1, "好きです付き合って下さい", "0", "10", null, null);
INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id") 
VALUES (26, 2, "愛してる俺の女になれ", "0", "10", null, null);
INSERT INTO answers(id, number,"body", "feeling_point", "question_id", "next_line_id", "next_question_id")
VALUES (27, 3, "好きだ。俺と付き合ってくれるか", "0", "10", null, null);
