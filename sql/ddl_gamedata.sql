DROP TABLE IF EXISTS gamedata;
CREATE TABLE gamedata (id integer primary key autoincrement,
                      user_name varchar(20),
                      good_feeling integer default 0,
                      save_question_id integer,
                      save_line_id integer,
                      save_feeling_id integer);