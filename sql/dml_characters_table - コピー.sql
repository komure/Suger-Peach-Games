-- INSERT INTO characters(id, "name", "aa", "prefix", "suffix") VALUES (id, "name", "aa", "prefix", "suffix");

INSERT INTO characters(id, "name", "aa", "prefix", "suffix") VALUES (1, "@@@name@@@","  ", "「", "」");
INSERT INTO characters(id, "name", "aa", "prefix", "suffix") VALUES (2, "桃","
          @@@@@@@@
       p@@@@@@@@@@@q
      @8|  ^    ^  |8@
     @@|  (・) (・) |@@
    @@|      <      |@@
    @@| //   v    //|@@
    @@@L____________/@@@
     @@      | |    @@@
      p------ V ------q
      |               |", "「", "」");


INSERT INTO characters(id, "name", "aa", "prefix", "suffix") VALUES (3, "健太", "

        /////////////
       8|  ^    ^  |8
       |  (・) (・) |
     c|      <      |D
      | //    Ⅴ  // |
       L____________/
             | |
      p------ V ------q
      |               |", "「", "」");

INSERT INTO characters(id, "name", "aa", "prefix", "suffix") VALUES (4, "杉本先生", "
       /////////////
       8|  ＝  ＝  |8
       | ー□--□ー |
     c|      <      |D
      |  /   ー   ＼ |
       L____________/
             | |
      p------ V ------q
      |               |", "「", "」");
INSERT INTO characters(id, "name", "aa", "prefix", "suffix") VALUES (5, "咲夜", '
        @@@@@@@@@@@@
        @@@@@@@@@@@@
       8|  ^    ^  |8
       |  <・><・>  |
     c|       U     |D
      |  "   O    " |
       L____________/
             | |
      p------ V ------q
      |               |', "「", "」");
INSERT INTO characters(id, "name", "aa", "prefix", "suffix") VALUES (6, "", "", null, null);
INSERT INTO characters(id, "name", "aa", "prefix", "suffix") VALUES (7, "汐", "  ", "「", "」");
INSERT INTO characters(id, "name", "aa", "prefix", "suffix") VALUES (8, "？？？", "  ", "「", "」");
