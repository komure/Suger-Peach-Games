INSERT INTO lines(id, line, character_id, next_line_id) values(1, '校門まで残り２００メートル。

そこで立ち尽くす。

はぁ。

ため息と共に空を仰ぐ。
その先に校門はあった。
', 6, 2);
INSERT INTO lines(id, line, character_id, next_line_id) values(2, '誰が好んで、あんな場所に校門を据えたのか。
長い坂道が、悪夢のように延びていた。
', 6, 3);

INSERT INTO lines(id, line, character_id, next_line_id) values(3, 'はぁ・・・。', 6, 4);

INSERT INTO lines(id, line, character_id, next_line_id) values(4, '別のため息。俺のより小さく、短かかった。
隣をみてみる。', 6, 5);
INSERT INTO lines(id, line, character_id, next_line_id) values(5, 'そこに同じように立ち尽くす女の子がいた。

短い髪が、肩のすぐ上で風にそよいでいる。', 6, 6);

INSERT INTO lines(id, line, character_id, next_line_id) values(6, 'この学校は、好きですか。

え・・・？', 6, 7);
INSERT INTO lines(id, line, character_id, next_line_id) values(7, 'いや、俺に訊いているのではなかった。', 6, 8);
INSERT INTO lines(id, line, character_id, next_line_id) values(8, 'わたしはとってもとっても好きです。
でも、なにもかも・・・変わらずにはいられないです。
楽しいこととか、うれしいこととか、ぜんぶ。
・・・ぜんぶ、変わらずにはいられないです。', 6, 9);
INSERT INTO lines(id, line, character_id, next_line_id) values(9, 'たどたどしく、ひとり言を続ける。', 6, 10);
INSERT INTO lines(id, line, character_id, next_line_id) values(10, 'それでも、この場所が好きでいられますか。', 6, 11);
INSERT INTO lines(id, line, character_id, next_line_id) values(11, 'わたしは・・・', 6, 12);
INSERT INTO lines(id, line, character_id, next_line_id) values(12, '見つければいいだけだろ。', 6, 13);
INSERT INTO lines(id, line, character_id, next_line_id) values(13, 'えっ・・・？', 6, 14);
INSERT INTO lines(id, line, character_id, next_line_id) values(14, '驚いて、俺の顔を見る。', 6, 15);
INSERT INTO lines(id, line, character_id, next_line_id) values(15, '次の楽しいこととか、うれしいことを見つければいいだけだろ。
あんたの楽しいことや、うれしいことはひとつだけなのか？違うだろ。', 6, 16);

INSERT INTO lines(id, line, character_id, next_line_id) values(16, 'そう。

何も知らなかった無垢な頃。

誰にでもある。', 6, 17);
INSERT INTO lines(id, line, character_id, next_line_id) values(17, 'ほら、いこうぜ。', 6, 18);

INSERT INTO lines(id, line, character_id, next_line_id) values(18, '俺たちは登り始める。

長い、長い坂道を。', 6, 19);
INSERT INTO lines(id, line, character_id, next_line_id) values(19, '～学校～', 6, 20);
INSERT INTO lines(id, line, character_id, next_line_id) values(20, '@@@name@@@！同じクラスだな！', 8, 21);
INSERT INTO lines(id, line, character_id, next_line_id) values(21, 'ん？誰だお前は。無視無視', 1, 22);
INSERT INTO lines(id, line, character_id, next_line_id) values(22, 'もー冗談はやめてくれよー。藤田健太だよ～。
小学校からの仲だろ～？', 8, 23);
INSERT INTO lines(id, line, character_id, next_line_id) values(23, 'はいはい。高校まで一緒とは呪われてんな', 1, 24);
INSERT INTO lines(id, line, character_id, next_line_id) values(24, 'ひどいな～', 3, 25);
INSERT INTO lines(id, line, character_id, next_line_id) values(25, 'はい、皆さん席について下さい。私がこのクラスの担任、杉本です。宜しくお願いします', 8, 26);
INSERT INTO lines(id, line, character_id, next_line_id) values(26, 'ほー。
地元なら誰でも知ってる名物教師、杉本先生がさっそくクラスの担任とは。', 6, 27);
INSERT INTO lines(id, line, character_id, next_line_id) values(27, 'この学校のルールは私が全て決めています。例えば・・・云々・・・・・。ということで名門大学に入学志望の方は私にまで。', 4, 28);
INSERT INTO lines(id, line, character_id, next_line_id) values(28, '～放課後～', 6, 29);
INSERT INTO lines(id, line, character_id, next_line_id) values(29, 'はぁ、疲れた。
ホームルーム中に居眠りしてたら、いつのまにか図書委員にされていた。
図書室の先生がグダグダ言っている間にもうこんな時間だぜ・・・。', 6, 30);
INSERT INTO lines(id, line, character_id, next_line_id) values(30, 'カバン取って早く帰ろ。', 6, 31);

INSERT INTO lines(id, line, character_id, next_line_id) values(31, '～教室～', 6, 32);
INSERT INTO lines(id, line, character_id, next_line_id) values(32, 'ガラガラ', 6, 33);
INSERT INTO lines(id, line, character_id, next_line_id) values(33, 'ん？
見覚えのある人物が教室にいる。
ああ、今朝校門で会った子か。同じクラスだったんだな。', 6, 34);
INSERT INTO lines(id, line, character_id, next_line_id) values(34, 'こんな時間までなにしてるんだ', 1, 35);
INSERT INTO lines(id, line, character_id, next_line_id) values(35, 'あっ、はい、飼育員の仕事をしていました', 8, 36);
INSERT INTO lines(id, line, character_id, next_line_id) values(36, 'そうだったのか。えっと名前は・・・', 1, 37);
INSERT INTO lines(id, line, character_id, next_line_id) values(37, '佐藤桃です', 8, 38);
INSERT INTO lines(id, line, character_id, next_line_id) values(38, '俺は@@@name@@@だ。よろしくな', 1, 39);
INSERT INTO lines(id, line, character_id, next_line_id) values(39, 'よろしくお願いします', 2, 40);
INSERT INTO lines(id, line, character_id, next_line_id) values(40, 'そーいや何食ってるんだ？', 1, 41);
INSERT INTO lines(id, line, character_id, next_line_id) values(41, 'あんぱんですっ', 2, 42);
INSERT INTO lines(id, line, character_id, next_line_id) values(42, 'お菓子な奴だな。', 6, 43);
INSERT INTO lines(id, line, character_id, next_line_id) values(43, '・・・・・・・・・・・・・・・・・・・・・・・。', 6, 44);
INSERT INTO lines(id, line, character_id, next_line_id) values(44, 'なんだかんだで４月はあっという間に終わった。', 6, 45);
INSERT INTO lines(id, line, character_id, next_line_id) values(45, '～ゴールデンウィーク～', 6, 46);
INSERT INTO lines(id, line, character_id, next_line_id) values(46, '高校生活が始まって初めての大型連休だ。
健太と偶然同じクラスになれたのはラッキーだけど、新しい友達を作るのって意外と疲れるな。
この連休でしっかり疲れを取らないとだな。', 6, 47);
INSERT INTO lines(id, line, character_id, next_line_id) values(47, 'もう１１時かー。', 6, 48);
INSERT INTO lines(id, line, character_id, next_line_id) values(48, '家でゴロゴロするのもいいけど、どっか出かけるか！
とりあえず誰か誘ってみるか。', 6, null);
INSERT INTO lines(id, line, character_id, next_line_id) values(49, 'こんにちは先生。今大丈夫っすか？', 1, 50);
INSERT INTO lines(id, line, character_id, next_line_id) values(50, 'どうしたんですか急に？今から愛犬の散歩に行こうと思ってたんですよ。', 4, 51);
INSERT INTO lines(id, line, character_id, next_line_id) values(51, 'いや、その何ていうか、、、俺あんまり友達もいないんで、気づいたら先生に電話してたんです', 1, 52);
INSERT INTO lines(id, line, character_id, next_line_id) values(52, 'なるほど。そういうことなら、君も一緒に散歩にきますか？', 4, 53);
INSERT INTO lines(id, line, character_id, next_line_id) values(53, 'いいんですか？じゃあ今から行きます！', 1, 54);
INSERT INTO lines(id, line, character_id, next_line_id) values(54, '～公園～', 6, 55);
INSERT INTO lines(id, line, character_id, next_line_id) values(55, 'こんにちは先生', 1, 56);
INSERT INTO lines(id, line, character_id, next_line_id) values(56, 'こんにちは。ところで君、勉強は好きですか？', 4, 57);
INSERT INTO lines(id, line, character_id, next_line_id) values(57, 'え・・・？', 1, 58);
INSERT INTO lines(id, line, character_id, next_line_id) values(58, '私は多くの生徒を名門大学に送り出してきました。そして君にはその素質があると思います。', 4, 59);
INSERT INTO lines(id, line, character_id, next_line_id) values(59, '・・・・・。', 1, 60);
INSERT INTO lines(id, line, character_id, next_line_id) values(60, 'どうですか。私と東大合格を目指しませんか？', 4, 61);
INSERT INTO lines(id, line, character_id, next_line_id) values(61, '冗談はやめてくださいよ！俺そんな素質ないですよ！だいたい中学の授業だってろくに受けてないのに・・・。', 1, 62);
INSERT INTO lines(id, line, character_id, next_line_id) values(62, '今からでも十分間に合いますよ。君の眼が私にそう教えてくれている。', 4, 63);
INSERT INTO lines(id, line, character_id, next_line_id) values(63, '先生・・・', 1, 64);
INSERT INTO lines(id, line, character_id, next_line_id) values(64, 'こうして俺は先生と一緒に東大合格を目指すこととなった。', 6, 65);
INSERT INTO lines(id, line, character_id, next_line_id) values(65, '中学生時代はどうしようもないクソガキで、テストだってボロボロだったけど
先生の見込み通り、実は俺には勉強という才能があるらしい。
テストの平均点は９０点越えをキープ。生徒会長にもなった。', 6, 66);
INSERT INTO lines(id, line, character_id, next_line_id) values(66, 'そして俺は・・・・', 6, 67);
INSERT INTO lines(id, line, character_id, next_line_id) values(67, '見事、東京大学に合格した。', 6, 68);
INSERT INTO lines(id, line, character_id, next_line_id) values(68, '勉強に専念した３年間、彼女ができることも、デートに行くことすらなかった俺だが
それはそれでよかったのかもしれない。', 6, 69);
INSERT INTO lines(id, line, character_id, next_line_id) values(69, '杉本先生、ありがとう。', 6, 70);
INSERT INTO lines(id, line, character_id, next_line_id) values(70, 'バッドエンド　～完～', 6, null);
INSERT INTO lines(id, line, character_id, next_line_id) values(71, 'よう健太！', 1, 72);
INSERT INTO lines(id, line, character_id, next_line_id) values(72, 'は～あい。どちらさん？', 3, 73);
INSERT INTO lines(id, line, character_id, next_line_id) values(73, '俺だよ。今日お前暇か？', 1, 74);
INSERT INTO lines(id, line, character_id, next_line_id) values(74, 'なんだお前か。ああ、俺は今日忙しいぜ。ゲーセン行くって用事があるからさ', 3, 75);
INSERT INTO lines(id, line, character_id, next_line_id) values(75, 'ってようするに暇なんだろ？じゃあ俺も行くから、いつもんとこで待ち合わせな', 1, 76);
INSERT INTO lines(id, line, character_id, next_line_id) values(76, 'へいへい', 3, 77);
INSERT INTO lines(id, line, character_id, next_line_id) values(77, '～ゲームセンター～', 6, 78);
INSERT INTO lines(id, line, character_id, next_line_id) values(78, 'よう健太！', 1, 79);
INSERT INTO lines(id, line, character_id, next_line_id) values(79, 'おっす', 3, 80);
INSERT INTO lines(id, line, character_id, next_line_id) values(80, 'そういやこのゲーセン、新しいクレーンマシーン入ったみたいだな。Louis Vuittonの財布が手に入るかもらしいぜ。', 1, 81);
INSERT INTO lines(id, line, character_id, next_line_id) values(81, 'へーそうなんだ。んじゃいっちょやってみるか？', 3, null);
INSERT INTO lines(id, line, character_id, next_line_id) values(82, '（２０回目...３０回目...４０回目...)', 6, 83);
INSERT INTO lines(id, line, character_id, next_line_id) values(83, '・・・くっそ', 1, 84);
INSERT INTO lines(id, line, character_id, next_line_id) values(84, 'なんだよーじぇんじぇん取れないじゃんー', 3, 85);
INSERT INTO lines(id, line, character_id, next_line_id) values(85, 'うるせい！あと一回！', 1, 86);
INSERT INTO lines(id, line, character_id, next_line_id) values(86, '・・・・・・・・・・・・・・・・・・・・・・・。', 6, 87);
INSERT INTO lines(id, line, character_id, next_line_id) values(87, '結局取れなかったじゃんかよ。クソじゃんお前。', 3, 88);
INSERT INTO lines(id, line, character_id, next_line_id) values(88, 'くそっ！今月の小遣いほぼ使っちまったよ', 1, 89);
INSERT INTO lines(id, line, character_id, next_line_id) values(89, 'バイトでもしろよ。もうそろそろ行こうぜ。俺お腹減っちまったよ', 3, 90);
INSERT INTO lines(id, line, character_id, next_line_id) values(90, 'そうだな。', 1, 91);
INSERT INTO lines(id, line, character_id, next_line_id) values(91, '～駅前～', 6, 92);
INSERT INTO lines(id, line, character_id, next_line_id) values(92, 'あれ？あそこにいんの佐藤じゃね？', 3, 133);
INSERT INTO lines(id, line, character_id, next_line_id) values(93, '（１回目...２回目...３回目...)', 6, 94);
INSERT INTO lines(id, line, character_id, next_line_id) values(94, 'イェーイ！！！！取れたぜ！！！！', 3, 95);
INSERT INTO lines(id, line, character_id, next_line_id) values(95, 'まじかよ！お前すげえな！', 1, 96);
INSERT INTO lines(id, line, character_id, next_line_id) values(96, 'はっはっは！俺様にかかればこんなもん楽勝だぜ！', 3, 97);
INSERT INTO lines(id, line, character_id, next_line_id) values(97, '俺も財布欲しいからやってみようかなー', 1, 98);
INSERT INTO lines(id, line, character_id, next_line_id) values(98, 'ビトンだっけ？俺こんなん使わないからお前にやるよ。良き友を持ったな。はっはっは。', 3, 99);
INSERT INTO lines(id, line, character_id, next_line_id) values(99, 'え！いいのか？ラッキー！', 1, 100);
INSERT INTO lines(id, line, character_id, next_line_id) values(100, 'さて、もうそろそろ行こうぜ。俺お腹減っちまったよ', 3, 101);
INSERT INTO lines(id, line, character_id, next_line_id) values(101, 'そうだな。', 1, 102);
INSERT INTO lines(id, line, character_id, next_line_id) values(102, '～駅前～', 6, 103);
INSERT INTO lines(id, line, character_id, next_line_id) values(103, 'あれ？あそこにいんの佐藤じゃね？', 3, 133);
INSERT INTO lines(id, line, character_id, next_line_id) values(104, 'おっす！佐藤', 1, 105);
INSERT INTO lines(id, line, character_id, next_line_id) values(105, 'こんにちは@@@name@@@くん、どうしたんですか？
', 2, 106);
INSERT INTO lines(id, line, character_id, next_line_id) values(106, 'あ、いや、別に用があるわけではないんだけど、暇だなって思って・・・', 1, 107);
INSERT INTO lines(id, line, character_id, next_line_id) values(107, '私も今日は暇してて、珍動物園の無料券もらったから行こうと思ってたところなんですけど、@@@name@@@くんも一緒にどうですか？', 2, 108);
INSERT INTO lines(id, line, character_id, next_line_id) values(108, 'いいのか？じゃあ行くわ。動物園の前で待ち合わせな', 1, 109);
INSERT INTO lines(id, line, character_id, next_line_id) values(109, 'はい！', 2, 110);
INSERT INTO lines(id, line, character_id, next_line_id) values(110, '電話したのは自分なはずなのに、なんか凄く緊張するな・・・。
女の子と二人で出かけるのっていつぶりだっけ？', 6, 111);
INSERT INTO lines(id, line, character_id, next_line_id) values(111, '～動物園～', 6, 112);
INSERT INTO lines(id, line, character_id, next_line_id) values(112, 'よう佐藤', 1, 113);
INSERT INTO lines(id, line, character_id, next_line_id) values(113, 'こんにちは', 2, 114);
INSERT INTO lines(id, line, character_id, next_line_id) values(114, '動物園なんてガキのころ以来だよ', 1, 115);
INSERT INTO lines(id, line, character_id, next_line_id) values(115, '私は結構動物園好きなんです。特にあの動物・・・', 2, 116);
INSERT INTO lines(id, line, character_id, next_line_id) values(116, 'あの動物？？', 1, 117);
INSERT INTO lines(id, line, character_id, next_line_id) values(117, 'えへへ。ヒミツ', 2, 118);
INSERT INTO lines(id, line, character_id, next_line_id) values(118, '大型連休だけあって、子連れ家族がたくさんいるな。
動物を見るだけであんなにテンションあがるのか。ははは。
俺にもこんな時代があったのかな。', 6, 119);
INSERT INTO lines(id, line, character_id, next_line_id) values(119, '春の気候になってきましたけど、やっぱりちょっとまだ寒いですね', 2, null);
INSERT INTO lines(id, line, character_id, next_line_id) values(120, 'ん？寒いか？じゃあ温かい飲み物でも買ってくるから、ちょっと待っててくれよ', 1, 121);
INSERT INTO lines(id, line, character_id, next_line_id) values(121, 'えーっと、佐藤は何が好きなんだろう。
ホットココアでいいかな？俺はコーンスープっと。', 6, 122);
INSERT INTO lines(id, line, character_id, next_line_id) values(122, 'おまたせ。ホットココアでよかった？', 1, 123);
INSERT INTO lines(id, line, character_id, next_line_id) values(123, 'わーあったかい。ありがとうございます@@@name@@@くん', 2, 124);
INSERT INTO lines(id, line, character_id, next_line_id) values(124, 'どういたしまして。それじゃ何か見にいこうぜ', 1, null);
INSERT INTO lines(id, line, character_id, next_line_id) values(125, 'じゃあとりあえず何か見に行こうぜ', 1, null);
INSERT INTO lines(id, line, character_id, next_line_id) values(126, 'ライオン！！！', 2, 129);

INSERT INTO lines(id, line, character_id, next_line_id) values(127, 'パンダ！！！！！', 2, 129);
INSERT INTO lines(id, line, character_id, next_line_id) values(128, 'イグアナ！！！！！！！！！！', 2, 129);
INSERT INTO lines(id, line, character_id, next_line_id) values(129, '俺たちは結局閉園時間まで歩き回った。
最後まで佐藤の好きな動物は教えてもらえなかったけど、きっとアレだ。
ははは。変わってるな。個人的には、カピバラの入浴が一番可愛かったと思う。', 6, 130);
INSERT INTO lines(id, line, character_id, next_line_id) values(130, '佐藤もずっと嬉しそうにしてたし、楽しんでくれたと思う。
今日は動物園に来れてよかった。', 6, 131);
INSERT INTO lines(id, line, character_id, next_line_id) values(131, '～帰り道・駅前～', 6, 132);
INSERT INTO lines(id, line, character_id, next_line_id) values(132, 'あれ？あそこのいるのは藤田くんじゃないですか？', 2, 133);
INSERT INTO lines(id, line, character_id, next_line_id) values(133, 'おっ奇遇だな', 1, 134);
INSERT INTO lines(id, line, character_id, next_line_id) values(134, 'おっす！ゲーセン最高！学校がないってホントサイコー！！！！！毎日ゲーセン登校したいぜ。', 3, 135);
INSERT INTO lines(id, line, character_id, next_line_id) values(135, 'ははは、私は学校も好きです', 2, 136);
INSERT INTO lines(id, line, character_id, next_line_id) values(136, '偶然駅前で遭遇し、俺たちは他愛もない話をしながら一緒に帰った。
健太の妹が家出をして、まだ身元不明だという話。佐藤の家が喫茶店を営んでる話。俺が最近金魚と鯉を飼い始めた話。金魚の名前は、カズキ。鯉の名前は、ユウタに決定した。', 6, 137);
INSERT INTO lines(id, line, character_id, next_line_id) values(137, 'そういえば、健太が言っていたがゴールデンウィーク明けに新しく転校生が来るらしい。
どんな奴なんだろう。', 6, 138);
INSERT INTO lines(id, line, character_id, next_line_id) values(138, '～学校・昼休み～', 6, 139);
INSERT INTO lines(id, line, character_id, next_line_id) values(139, 'いつも通り、昼飯を食うために屋上に向かった。
この時間が学校のストレスから解放される唯一の楽しみだ。今日の昼飯は購買で買った、焼きそばパンと鶯パンだ。', 6, 140);
INSERT INTO lines(id, line, character_id, next_line_id) values(140, 'ガチャン', 6, 141);
INSERT INTO lines(id, line, character_id, next_line_id) values(141, 'ドアを開けると、そこには先客がいた。
ダーツの練習？をしている・・・・・・・・？。', 6, 142);
INSERT INTO lines(id, line, character_id, next_line_id) values(142, 'やはりこの角度が安定しているか。さすが僕だ。はっはっはっはっは。', 8, null);
INSERT INTO lines(id, line, character_id, next_line_id) values(143, 'ふっ。僕は学校という枠組みには捕らわれたくない主義でね。いつでもフリーダム。好きな言葉はカジュアルさ。', 8, 144);
INSERT INTO lines(id, line, character_id, next_line_id) values(144, '・・・・・・・・・。', 1, 145);
INSERT INTO lines(id, line, character_id, next_line_id) values(145, 'すまない、自己紹介が遅れた。僕の名前は、鬼龍院咲夜だ。カッコイイ名前だろ？よく源氏名と間違えられるけど、本名なんだze。', 8, 146);
INSERT INTO lines(id, line, character_id, next_line_id) values(146, 'ああ、転校生の', 1, 147);
INSERT INTO lines(id, line, character_id, next_line_id) values(147, '今話題のイケメン転校生っていったとこかな。ははは。ところで、佐藤桃って知っているかい？僕は彼女を追いかけて転校してきたんだ。ダーツがひと段落したからね。
桃って凄く可愛いだろ？', 5, null);
INSERT INTO lines(id, line, character_id, next_line_id) values(148, 'はっはっは。君は本当に見る目がないね。', 5, 151);
INSERT INTO lines(id, line, character_id, next_line_id) values(149, 'ま、君には関係のないことだけどね。桃は僕のお嫁さんになるんだからさ', 5, 151);
INSERT INTO lines(id, line, character_id, next_line_id) values(150, 'なんだい？僕というライバルができて取り乱しているのかい？', 5, 151);
INSERT INTO lines(id, line, character_id, next_line_id) values(151, '鬼龍院咲夜・・・・。', 6, 152);
INSERT INTO lines(id, line, character_id, next_line_id) values(152, 'なんかすごい面倒くさそうな奴だ。
あまり関わりたくないな・・・。', 6, 153);
INSERT INTO lines(id, line, character_id, next_line_id) values(153, '・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・', 6, 154);
INSERT INTO lines(id, line, character_id, next_line_id) values(154, '・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・', 6, 155);
INSERT INTO lines(id, line, character_id, next_line_id) values(155, '俺の学校生活は何だかんだ充実していた。
勉強は今まで通りボロボロだったけど、図書委員の仕事をしたり、放課後や休日に健太と佐藤と遊びに行ったりした。', 6, 156);
INSERT INTO lines(id, line, character_id, next_line_id) values(156, '佐藤は体が弱いらしく、長期間で学校を休んでたりしたが
お見舞いに行くといつも笑顔で迎え入れてくれた。', 6, 157);
INSERT INTO lines(id, line, character_id, next_line_id) values(157, '学校の帰り道に、佐藤の家の前でコソコソしている咲夜を何度か見かけたが・・・・・・・。
それについては触れないでおこう。', 6, 158);

INSERT INTO lines(id, line, character_id, next_line_id) values(158, '・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・', 6, 159);
INSERT INTO lines(id, line, character_id, next_line_id) values(159, '・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・', 6, 160);
INSERT INTO lines(id, line, character_id, next_line_id) values(160, '～夏休み～', 6, 161);
INSERT INTO lines(id, line, character_id, next_line_id) values(161, 'ミーンミンミンミン　ミーンミンミンミン', 6, 162);
INSERT INTO lines(id, line, character_id, next_line_id) values(162, 'せっかくの夏休だっていうのに、補修だ・・・。
ノー勉で赤点３つも取ったから当たり前っちゃ当たり前だけど・・・。
まあ、昼過ぎには終わるっぽいし、補修の後ゲーセンでも行くか。', 6, 163);
INSERT INTO lines(id, line, character_id, next_line_id) values(163, 'にしても、健太の野郎ノー勉とか言っといてそこそこの点数取ってやがるし。
いるよな、ノー勉って言ってるけど実はやってる奴。
俺は、ノー勉って言ったら百パーセントノー勉だけどな。', 6, 164);
INSERT INTO lines(id, line, character_id, next_line_id) values(164, '・・・そんなん自慢にはならねーか。', 6, 165);
INSERT INTO lines(id, line, character_id, next_line_id) values(165, '～学校～', 6, 166);
INSERT INTO lines(id, line, character_id, next_line_id) values(166, '@@@name@@@くん', 8, 167);
INSERT INTO lines(id, line, character_id, next_line_id) values(167, '聞き慣れた声が俺を呼ぶ。
佐藤だった。', 6, 168);
INSERT INTO lines(id, line, character_id, next_line_id) values(168, '@@@name@@@くんも補修ですか？', 2, 169);
INSERT INTO lines(id, line, character_id, next_line_id) values(169, 'おっす佐藤。そうなんだよ、赤点３つも取っちまって。佐藤も補修なのか？', 1, 170);
INSERT INTO lines(id, line, character_id, next_line_id) values(170, '恥ずかしながら、私は赤点４つも取ってしまいました。あちゃちゃ。', 2, 171);
INSERT INTO lines(id, line, character_id, next_line_id) values(171, '以外だな。佐藤が赤点なんて', 1, 172);
INSERT INTO lines(id, line, character_id, next_line_id) values(172, 'しばらく学校をお休みしていたので、試験勉強ができませんでした', 2, 173);
INSERT INTO lines(id, line, character_id, next_line_id) values(173, 'もう具合は大丈夫なのか？', 1, 174);
INSERT INTO lines(id, line, character_id, next_line_id) values(174, 'はい！大丈夫です', 2, 175);
INSERT INTO lines(id, line, character_id, next_line_id) values(175, 'おはようございます。はい、それでは皆さん席について下さい。今日の補修を始めたいと思います。', 4, 176);
INSERT INTO lines(id, line, character_id, next_line_id) values(176, '～学校・午後～', 6, 177);
INSERT INTO lines(id, line, character_id, next_line_id) values(177, 'ふー。やっと終わった', 1, 178);
INSERT INTO lines(id, line, character_id, next_line_id) values(178, 'たくさん頭を使ったので、何か甘いものが食べたいです', 2, 179);
INSERT INTO lines(id, line, character_id, next_line_id) values(179, 'それならこれから海行って、かき氷でも食うか？', 1, 180);
INSERT INTO lines(id, line, character_id, next_line_id) values(180, 'はい！行きたいです！イチゴ味！', 2, 181);
INSERT INTO lines(id, line, character_id, next_line_id) values(181, '～海～', 6, 182);
INSERT INTO lines(id, line, character_id, next_line_id) values(182, 'というわけで、俺たちは補修の後二人で海に行くことになった。
炎天下の中、色白の佐藤を連れまわすわけには行かないので、早々に海の家に行った。
佐藤はイチゴ味（練乳入り）のかき氷。俺は宇治金時（あんこ大盛り）を頼んだ。', 6, 183);
INSERT INTO lines(id, line, character_id, next_line_id) values(183, '@@@name@@@くんのも一口下さい！', 2, 184);
INSERT INTO lines(id, line, character_id, next_line_id) values(184, '・・・こんな時間がいつまでも続けばいいのに。', 6, 185);
INSERT INTO lines(id, line, character_id, next_line_id) values(185, '俺がトイレに行っている間に、佐藤はナンパにあっていたらしい。
三人組がつまらなそうに海の家から出ていくのが見えた。', 6, 186);
INSERT INTO lines(id, line, character_id, next_line_id) values(186, '佐藤は・・・
大きな瞳に涙を浮かべて俯いていた。', 6, 187);
INSERT INTO lines(id, line, character_id, next_line_id) values(187, '@@@name@@@くん・・・・', 2, 188);
INSERT INTO lines(id, line, character_id, next_line_id) values(188, 'どうした？大丈夫か？ナンパされたのか？', 1, 189);
INSERT INTO lines(id, line, character_id, next_line_id) values(189, '凄く怖かったです。砂浜に蟻地獄作ったから入ってみてくれって言われました・・・', 2, 190);
INSERT INTO lines(id, line, character_id, next_line_id) values(190, '（蟻地獄？？？）', 6, 191);
INSERT INTO lines(id, line, character_id, next_line_id) values(191, '凄く大きな蟻地獄らしいです・・・。', 2, 192);
INSERT INTO lines(id, line, character_id, next_line_id) values(192, 'そ、それは大変だったな', 1, 193);
INSERT INTO lines(id, line, character_id, next_line_id) values(193, '蟻地獄勧誘事件は意味が分からなかったが
追加注文したおでんを食べたら佐藤も落ち着きを取り戻したので
とりあえず良かった？かな。', 6, 194);
INSERT INTO lines(id, line, character_id, next_line_id) values(194, '～海岸～', 6, 195);
INSERT INTO lines(id, line, character_id, next_line_id) values(195, 'おー凄い綺麗な夕焼けだな・・・。', 1, 196);
INSERT INTO lines(id, line, character_id, next_line_id) values(196, 'はい・・・。凄く綺麗です・・・。', 2, 197);
INSERT INTO lines(id, line, character_id, next_line_id) values(197, '日も暮れてきたし、そろそろ帰るか？', 1, 198);
INSERT INTO lines(id, line, character_id, next_line_id) values(198, '・・・・。', 2, 199);
INSERT INTO lines(id, line, character_id, next_line_id) values(199, 'どうした？', 1, 200);
INSERT INTO lines(id, line, character_id, next_line_id) values(200, 'まだ帰りたくないです', 2, 201);
INSERT INTO lines(id, line, character_id, next_line_id) values(201, 'えっ', 1, 202);
INSERT INTO lines(id, line, character_id, next_line_id) values(202, 'もう少し帰りたくないです・・・', 2, 203);
INSERT INTO lines(id, line, character_id, next_line_id) values(203, 'そ、そうか。じゃあ、今日は杉本神社で祭りやってるみたいだから行ってみるか？', 1, 204);
INSERT INTO lines(id, line, character_id, next_line_id) values(204, 'はい！行きたいです！リンゴ飴！', 2, 205);
INSERT INTO lines(id, line, character_id, next_line_id) values(205, 'そんなわけで、俺たちは祭りに行くことになった。', 1, 206);
INSERT INTO lines(id, line, character_id, next_line_id) values(206, '杉本神社は名前の通り、杉本先生の実家だ。', 6, 207);
INSERT INTO lines(id, line, character_id, next_line_id) values(207, '地元では結構有名な祭りだから、ガキのころから毎年健太と一緒に行っていた。
今年はこの間ナンパしたやつとデートだとか言ってたから、一緒に祭りに行くやつはいないと思ってたけど
佐藤と行けるなら、むしろ誘ってこなかった健太に感謝したい。', 6, 208);
INSERT INTO lines(id, line, character_id, next_line_id) values(208, '～祭り～', 6, 209);
INSERT INTO lines(id, line, character_id, next_line_id) values(209, '凄い人だな・・・。', 6, null);
INSERT INTO lines(id, line, character_id, next_line_id) values(210, '佐藤、逸れないように', 1, 211);
INSERT INTO lines(id, line, character_id, next_line_id) values(211, '佐藤の手小さいな・・・。', 6, 212);
INSERT INTO lines(id, line, character_id, next_line_id) values(212, '@@@name@@@くんの手あったかいです・・・', 2, 217);
INSERT INTO lines(id, line, character_id, next_line_id) values(213, '佐藤、逸れないように', 1, 214);
INSERT INTO lines(id, line, character_id, next_line_id) values(214, 'は、はい！ありがとうございますっ', 2, 217);
INSERT INTO lines(id, line, character_id, next_line_id) values(215, '逸れないように俺についてきてくれ！', 1, 216);
INSERT INTO lines(id, line, character_id, next_line_id) values(216, 'わかりました！', 2, 217);
INSERT INTO lines(id, line, character_id, next_line_id) values(217, '俺たちは参道に設置されている屋台を歩き回った。
佐藤は楽しみにしていたリンゴ飴を食べれて満足そうだ。ついでにブドウ飴も買ってあげた。', 6, 218);
INSERT INTO lines(id, line, character_id, next_line_id) values(218, '金魚すくいで取った金魚は、責任をもって俺が飼おう。
名前は・・・そうだな、モエにしよう。', 6, 219);
INSERT INTO lines(id, line, character_id, next_line_id) values(219, 'そろそろ花火が始まるみたいですね', 2, 220);
INSERT INTO lines(id, line, character_id, next_line_id) values(220, 'そうだな。実は花火鑑賞の穴場があるんだ。行ってみようか。', 1, 221);
INSERT INTO lines(id, line, character_id, next_line_id) values(221, '～花火～', 6, 222);
INSERT INTO lines(id, line, character_id, next_line_id) values(222, 'ヒュー　バーン　ヒュー　バーン', 6, 223);
INSERT INTO lines(id, line, character_id, next_line_id) values(223, '・・・・・綺麗ですね', 2, 224);
INSERT INTO lines(id, line, character_id, next_line_id) values(224, '・・・ああ。', 1, 225);
INSERT INTO lines(id, line, character_id, next_line_id) values(225, '何だか不思議な気持ちになる。
今までに味わったことのない、この胸が引き締められる感じ。', 6, 226);
INSERT INTO lines(id, line, character_id, next_line_id) values(226, '触れられる距離に佐藤がいて、一緒に花火を見ている。', 6, 227);
INSERT INTO lines(id, line, character_id, next_line_id) values(227, 'ずっとこうしていたい。', 6, 228);
INSERT INTO lines(id, line, character_id, next_line_id) values(228, '来年も、再来年も、その先もずっと・・・', 6, null);
INSERT INTO lines(id, line, character_id, next_line_id) values(229, '・・・・・・・・・・・・・・・・。', 6, 230);
INSERT INTO lines(id, line, character_id, next_line_id) values(230, '@@@name@@@くん・・・', 2, 231);
INSERT INTO lines(id, line, character_id, next_line_id) values(231, '来年も一緒に来てくれるか？ここに花火を見に', 1, 232);
INSERT INTO lines(id, line, character_id, next_line_id) values(232, '・・・・・はい', 2, null);
INSERT INTO lines(id, line, character_id, next_line_id) values(233, '・・・・・・・・・・・・・・・・。', 6, 234);
INSERT INTO lines(id, line, character_id, next_line_id) values(234, '佐藤。', 1, 235);
INSERT INTO lines(id, line, character_id, next_line_id) values(235, '@@@name@@@くん・・・？', 2, 236);
INSERT INTO lines(id, line, character_id, next_line_id) values(236, '・・・・キスしてもいいか', 1, 237);
INSERT INTO lines(id, line, character_id, next_line_id) values(237, '・・・・・はい。
                                                                              ッチュ', 2, null);
INSERT INTO lines(id, line, character_id, next_line_id) values(238, 'ギュッ', 6, 239);
INSERT INTO lines(id, line, character_id, next_line_id) values(239, '@@@name@@@くん・・・？', 2, 240);
INSERT INTO lines(id, line, character_id, next_line_id) values(240, '・・・・・しばらくこうしててもいいか', 1, 241);
INSERT INTO lines(id, line, character_id, next_line_id) values(241, '・・・・・はい。', 2, null);
INSERT INTO lines(id, line, character_id, next_line_id) values(242, 'ギュッッ', 6, 243);
INSERT INTO lines(id, line, character_id, next_line_id) values(243, '・・・・私もぎゅってしていいですか？', 2, 244);
INSERT INTO lines(id, line, character_id, next_line_id) values(244, '・・・・・うん。して。', 1, null);
INSERT INTO lines(id, line, character_id, next_line_id) values(245, 'ッチュ
                                                                    ・・・・・・・・・・んっ', 2, null);
INSERT INTO lines(id, line, character_id, next_line_id) values(246, '・・・・・・桃。', 1, 247);
INSERT INTO lines(id, line, character_id, next_line_id) values(247, '・・・・・・@@@name@@@くん。', 2, null);

