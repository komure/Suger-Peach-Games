DROP TABLE IF EXISTS answers;
CREATE TABLE answers (id integer primary key autoincrement,
                      number integer,
                      body varchar(1000),
                      feeling_point integer,
                      question_id integer,
                      next_line_id integer,
                      next_question_id integer,
                      end_id integer,
                      foreign key(question_id) references questions(id),
                      foreign key(next_line_id) references lines(id),
                      foreign key(next_question_id) references questions(id));