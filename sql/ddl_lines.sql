DROP TABLE IF EXISTS lines;
CREATE TABLE lines (id integer primary key autoincrement,
                      line varchar(3000),
                      character_id integer,
                      next_line_id integer,
                      foreign key(character_id) references characters(id),
                      foreign key(next_line_id) references lines(id));