#!/bin/sh
sqlite3 suger_peach.db < sql/dcl.sql
sqlite3 suger_peach.db < sql/ddl_answers.sql
sqlite3 suger_peach.db < sql/ddl_characters.sql
sqlite3 suger_peach.db < sql/ddl_gamedata.sql
sqlite3 suger_peach.db < sql/ddl_lines.sql
sqlite3 suger_peach.db < sql/ddl_questions.sql
sqlite3 suger_peach.db < sql/dml_answers.sql
sqlite3 suger_peach.db < sql/dml_characters_table.sql
sqlite3 suger_peach.db < sql/dml_lines_table.sql
sqlite3 suger_peach.db < sql/dml_questions.sql
sqlite3 suger_peach.db < sql/dml_gamedata.sql
