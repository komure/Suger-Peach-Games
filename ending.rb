class Ending

  def self.true_end(user_name)

    ending_message = "・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・
・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・
今日は２０２８年８月１５日
？？？「パパ～！はやくはやく！」
#{user_name}「待ちなさい汐！」
桃「汐！走ったら危ないわよ！」
汐「はやくー！！花火始まっちゃう！」
バーン　ヒュー　バーン
俺たちはこの先もずっと・・・
登り続ける。
長い、長い坂道を。
シュガー・ピーチ　～Fin～"
    ending_message.each_line do |line|
      line.each_char do |char|
        print char
        sleep 0.05
      end
      gets
    end
    gets
  end

  def self.bad_end
    ending_message =  "・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・
・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・・
結局俺はふられてしまった。
何よりショックだったのは、あんなに思わせぶりをしたくせに咲夜を選んだことだ。
もう二度と女は信じない。
誰が初対面でクソ女かどうか見分けられるってんだ。
何でこうなったんだろう。
佐藤と出逢っていなかったら・・・。
もしかしたら、俺の人生は変わっていたかもしれないのに・・・。
シュガー・ピーチ　～Fin～"
    ending_message.each_line do |line|
      line.each_char do |char|
        print char
        sleep 0.05
      end
      gets
    end
    gets
  end
end

#Ending.true_end('加藤純一')
