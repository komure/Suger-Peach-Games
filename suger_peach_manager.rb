require 'rdbi'
require 'rdbi-driver-sqlite3'
require './gamedata'
require './ending'


class Suger_peach_manager
  #include GameData

  def initialize(db_name)
    @db_name = db_name
    @dbh = RDBI.connect( :SQLite3, :database => @db_name )
    @gamedata = GameData.new(@dbh)
    @ending_id = nil
    @is_true_end = false
  end

  def run
    loop do
      print "
□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
□□■■□■□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
□■□□■■□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
■□□□□■□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
■□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
■□□□□□□□□■■□□□■■□□□□■■■□■■□□□■■■□□□□■■■□■■□□□□
□■■□□□□□□□■□□□□■□□□■□□□■□□□□■□□□■□□□□□■■□□■□□□
□□□■■□□□□□■□□□□■□□□■□□□■□□□■□□□□□■□□□□■□□□■□□□
□□□□□■□□□□■□□□□■□□□■□□□■□□□■■■■■■■□□□□■□□□□□□□
■□□□□□■□□□■□□□□■□□□□■■■□□□□■□□□□□□□□□□■□□□□□□□
■□□□□□■□□□■□□□□■□□□■□□□□□□□■□□□□□□□□□□■□□□□□□□
■□□□□□■□□□■□□□□■□□□■■■■□□□□■□□□□□■□□□□■□□□□□□□
■■□□□■□□□□■□□□■■□□■□□□□■□□□□■□□□□■□□□□■□□□□□□□
■□■■■□□□□□□■■■□□■□■□□□□□■□□□□■■■■□□□■■■■■■□□□□
□□□□□□□□□□□□□□□□□□■□□□□□■□□□□□□□□□□□□□□□□□□□□□
□□□□□□□□□□□□□□□□□□□■■■■■□□□□□□□□□□□□□□□□□□□□□□
□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
□■■■■■□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□■■□□□□□□□
□□■□□□■□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□■□□□□□□□
□□■□□□□■□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□■□□□□□□□
□□■□□□□■□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□■□□□□□□□
□□■□□□□■□□□□■■■□□□□□□■■■■□□□□□■■■□■□□□■□■■■□□□
□□■□□□□■□□□■□□□■□□□□■□□□□■□□□■□□□■■□□□■■□□□■□□
□□■□□□■□□□■□□□□□■□□□□□□□□■□□■□□□□□■□□□■□□□□■□□
□□■■■■□□□□■■■■■■■□□□□■■■■■□□■□□□□□□□□□■□□□□■□□
□□■□□□□□□□■□□□□□□□□□■□□□□■□□■□□□□□□□□□■□□□□■□□
□□■□□□□□□□■□□□□□□□□■□□□□□■□□■□□□□□□□□□■□□□□■□□
□□■□□□□□□□■□□□□□■□□■□□□□□■□□■□□□□□■□□□■□□□□■□□
□□■□□□□□□□□■□□□□■□□■□□□□■■□□□■□□□□■□□□■□□□□■□□
□■■■■□□□□□□□■■■■□□□□■■■■□■■□□□■■■■□□□■■■□□■■■□
□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□

      -----------------------------------------------
      1. New Game
      2. Load
      3. End
      番号を選んで下さい(1,2,3):"
      num = gets.chomp
      puts
      case num
      when "1"
        @gamedata.input_user_name
        scene
        break if @is_true_end
      when "2"
        @gamedata.load
        scene
        break if @is_true_end
      when "3"
        puts "\nEnd"
        break
      else
      end
    end
    @dbh.disconnect
  end


  private
  def scene
    begin
    #繰り返し処理
      while @gamedata.line_id do
        show_line
      end
      if @gamedata.question_id
        #質問表示する
        question = @dbh.execute("select body, month from questions where id = #{@gamedata.question_id}").fetch(:first)
        puts  question[0]

        # DBから質問取得(from questions where id = #{question_id})
        sth = @dbh.execute("select id, number, body, feeling_point, next_line_id, next_question_id, end_id from answers where question_id = #{@gamedata.question_id};")
        answers = {}
        sth.each do |answer|
        puts "#{answer[1]}: #{answer[2]}"
          answers[answer[1]] = answer
        end

        # 選択肢の入力値を受け取ります
        #  answers = {:choose_number => answer}
        begin
          print "選択肢を選んで下さい："
          choose_number = gets.chomp.to_i
          puts
        end until answers.has_key?(choose_number)

        choose_answer = answers[choose_number]
        @gamedata.good_feeling += choose_answer[3]
        @gamedata.line_id = choose_answer[4]
        @gamedata.question_id = choose_answer[5]
        @ending_id = choose_answer[6]

        if @gamedata.question_id
          next_question = @dbh.execute("select month from questions where id = #{@gamedata.question_id}").fetch(:first)
          if question[1] != next_question[0]
            @gamedata.save
          end
        end
      end
    end while @gamedata.line_id || @gamedata.question_id
    case @ending_id
    when 1
      Ending.true_end(@gamedata.user_name)
      @is_true_end = true
    when 2
      Ending.bad_end()
      @is_true_end = false
    end

  end

  def show_line
    line = @dbh.execute("select c.aa, c.name, l.line, l.next_line_id, c.prefix, c.suffix from lines l inner join characters c on c.id = l.character_id where l.id = #{@gamedata.line_id}").fetch(:first)
    puts line[0]
    line[1].gsub!('@@@name@@@', "#{@gamedata.user_name}")
    line[2].gsub!('@@@name@@@', "#{@gamedata.user_name}")

    if line[1] != ""
      print line[1],line[4]
    end
    line[2].each_char do |char|
       print char
       #sleep 0.05
    end
    if line[1] != ""
      print line[5]
    end
    puts
    gets
    @gamedata.line_id = line[3]
  end
end

suger_peach_manager = Suger_peach_manager.new("suger_peach.db")
suger_peach_manager.run
