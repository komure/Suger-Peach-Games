require 'rdbi'
require 'rdbi-driver-sqlite3'


class GameData
  def initialize(dbh)
    #@db_name = db_name
    @dbh = dbh
    @good_feeling = 0
    @user_name = "ハッピー"
    @question_id = 1
    @line_id = 1
  end

  attr_accessor :good_feeling, :user_name, :question_id, :line_id

  def input_user_name
    print "
    =============
    ~Suger Peach~
    =============
  　ユーザー名を入力してください\n"
    @user_name = gets.chomp
    @dbh.execute("update gamedata set user_name = ? where id = 1", @user_name)
  end

  def save#(question_id, line_id)
    @dbh.execute("update gamedata set save_question_id = ?, save_line_id = ?, save_feeling_id = ? where id = 1", @question_id, @line_id, @good_feeling)
  end

  def load#(question_id, line_id, feeling_id)
    game_data = @dbh.execute("select user_name, save_feeling_id, save_question_id, save_line_id from gamedata where id = 1").fetch(:first)
    @user_name = game_data[0]
    @good_feeling = game_data[1]
    @question_id = game_data[2]
    @line_id = game_data[3]
  end

  def to_s
    "@good_feeling=#{@good_feeling}
@user_name=#{@user_name}
@question_id=#{@question_id}
@line_id=#{@line_id}"
  end
end

#gamedate = GameData.new("../suger_peach.db")

#gamedate.update_feeling_points

#gamedate.good_feeling += feeling_point
#gamedate.good_feeling
